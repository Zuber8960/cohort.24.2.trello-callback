
/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/


const fs = require('fs');
const path = require('path');


const cards = path.resolve('cards.json');


function problem3(listID, callback) {
    setTimeout(() => {
        fs.readFile(cards, "utf-8", (err, data) => {
            if (err) {
                console.error(err);
            } else {
                data = JSON.parse(data);
                callback(null, data[listID]);
            }
        })
    }, 2 * 1000);
}



module.exports = problem3;

