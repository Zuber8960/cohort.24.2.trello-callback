
/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/



const fs = require('fs');
const path = require('path');

const problem1 = require('./callback1.cjs');
const problem2 = require('./callback2.cjs');
const problem3 = require('./callback3.cjs');



function problem6(thanosID) {

    function mainFunction(callback, problem1, problem2, problem3){
        problem1(thanosID, (err,data) => {
            if(err){
                console.error(err);
            }else{
                console.log(data);
                problem2(thanosID, (err, data) => {
                    if(err){
                        console.error(err);
                    }else{
                        console.log(data);
                        const ids = data.map(list => {
                            return list.id;
                        });
                        const names = data.map(list => {
                            return list.name;
                        });
                        // console.log(ids);
                        callback(problem3, ids, names);
                    }
                })
            }
        })
    };

    mainFunction(cb, problem1, problem2, problem3);

    function cb(problem3, ids, names){
        for(let index = 0; index < ids.length; index++){
            // console.log(ids[index]);
            problem3(ids[index], (err, data) => {
                if(err){
                    console.log(err);
                }else{
                    console.log(names[index] , ":",data);
                }
            })
        }
    }

};

// problem6("mcu453ed");


module.exports = problem6;





