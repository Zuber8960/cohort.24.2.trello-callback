
/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/



const fs = require('fs');
const path = require('path');

const problem1 = require('./callback1.cjs');
const problem2 = require('./callback2.cjs');
const problem3 = require('./callback3.cjs');



function problem5(thanosID) {

    function mainFunction(callback, problem1, problem2, problem3){
        problem1(thanosID, (err,data) => {
            if(err){
                console.error(err);
            }else{
                console.log(data);
                problem2(thanosID, (err, data) => {
                    if(err){
                        console.error(err);
                    }else{
                        console.log(data);
                        const mindList = data.find(list => {
                            return list.name === "Mind";
                        })
                        const mindId = mindList.id;

                        const spaceList = data.find(list => {
                            return list.name === "Space";
                        })
                        const spaceId = spaceList.id;
                        // console.log(id);
                        callback(problem3, mindId, spaceId);
                    }
                })
            }
        })
    };

    mainFunction(cb, problem1, problem2, problem3);

    function cb(problem3, mindId, spaceId){
        problem3(mindId, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log("Mind :",data);
            }
        });
        problem3(spaceId, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log("Space :",data);
            }
        })
    }

};

// problem5("mcu453ed");


module.exports = problem5;




