/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
const path = require('path');
const fs = require('fs');

const boards = path.resolve('boards.json');

function problem1(boardID, callback) {
    setTimeout(() => {
        fs.readFile(boards, "utf-8", (err, data) => {
            if (err) {
                console.error(err);
            } else {
                data = JSON.parse(data);
                
                const result = data.find(board => {
                    return board.id === boardID;
                })
                callback(null, result);
            }
        });
    }, 2 * 1000);
}


// problem1("mcu453ed", cb);

// function cb(err, data){
//     if(err){
//         console.log(err);
//     }else{
//         console.log(data);
//     }
// };


module.exports = problem1;
