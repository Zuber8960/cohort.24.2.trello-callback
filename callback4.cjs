
/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/



const fs = require('fs');
const path = require('path');

const problem1 = require('./callback1.cjs');
const problem2 = require('./callback2.cjs');
const problem3 = require('./callback3.cjs');



function problem4(thanosID) {

    function mainFunction(callback, problem1, problem2, problem3){
        problem1(thanosID, (err,data) => {
            if(err){
                console.error(err);
            }else{
                console.log(data);
                problem2(thanosID, (err, data) => {
                    if(err){
                        console.error(err);
                    }else{
                        console.log(data);
                        const mindList = data.find(list => {
                            return list.name === "Mind";
                        })
                        const {id} = mindList;
                        // console.log(id)problem4;
                        callback(problem3, id);
                    }
                })
            }
        })
    };

    mainFunction(cb, problem1, problem2, problem3);

    function cb(problem3, id){
        problem3(id, (err, data) => {
            if(err){
                console.log(err);
            }else{
                console.log("Mind :" ,data);
            }
        })
    }

}

// problem4("mcu453ed");

module.exports = problem4;


