
/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const path = require('path');
const fs = require('fs');

const lists = path.resolve('lists.json');


function problem2(boardID, callback) {
    setTimeout(() => {
        fs.readFile(lists, "utf-8", (err, data) => {
            if (err) {
                console.error(err);
            } else {
                data = JSON.parse(data);

                callback(null, data[boardID]);
            }
        })
    }, 2 * 1000);
}


// problem2("mcu453ed", cb);

// function cb(err, data){
//     if(err){
//         console.error(err);
//     }else{
//         console.log(data);
//     }
// }


module.exports = problem2;
